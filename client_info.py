from dataclasses import dataclass


@dataclass
class ClientInfo:
    """
    A container to hold necessary information to communicate with a client device.
    """
    friendly_name: str
    ip_address: str

    def __init__(self, ip_address: str = "127.0.0.1", name: str = "Client"):
        self.ip_address = ip_address
        self.friendly_name = name
