import os
from pathlib import Path

from config_parser_adapter import ClientConfigParser


def test_set_get_key_with_no_file_given() -> None:
    """
    Tests that keys are able to be set and retrieved with no file specified.
    :return:
    """
    config = ClientConfigParser()
    config.set("TestKey", 1)
    test_value = config.get("TestKey")
    assert test_value == '1'

def test_get_gives_none_if_key_not_found() -> None:
    """
    Tests that None is returned when trying to get a key that isn't found.
    :return:
    """
    config = ClientConfigParser()
    get_result = config.get("not_going_to_be_in_the_config_haha")
    assert get_result is None

def test_default_config_is_create_when_it_does_not_exist() -> None:
    """
    Tests that a default config file is created when one doesn't exist.
    :return:
    """
    try:
        os.remove("default.conf")
    except FileNotFoundError:
        pass
    ClientConfigParser()
    assert Path("default.conf").exists()
