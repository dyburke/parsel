from client_info import ClientInfo


def test_client_info_name_default() -> None:
    """
    Test that the default name is Client
    :return:
    """
    client_info = ClientInfo()
    assert client_info.friendly_name == "Client"

def test_client_info_ip_default() -> None:
    """
    Test that default ip address is correct.
    :return:
    """
    client_info = ClientInfo()
    assert client_info.ip_address == "127.0.0.1"

def test_client_info_ip() -> None:
    """
    Test ip address is set correctly.
    :return:
    """
    client_info = ClientInfo("10.0.0.1")
    assert client_info.ip_address == "10.0.0.1"
    