import config_parser_adapter


def main() -> None:
    """
    The main entry point of the application when ran standalone.
    :return:
    """
    parser = config_parser_adapter.ClientConfigParser("default.conf")
    print(parser.get("HeartbeatInterval"))


if __name__ == '__main__':
    main()
