from configparser import ConfigParser
from os.path import exists
from pathlib import Path
from typing import Any


class ClientConfigParser:
    """
    Implementation of ClientConfig protocol using the ConfigParser module.
    """
    def __init__(self, config: str = ""):
        self.config = ConfigParser()
        self.config.optionxform = str
        self.default_config_file = "default.conf"
        self.section = "Main"
        if not config:
            if not exists(self.default_config_file):
                Path(self.default_config_file).touch()
                self.create_default_config()
                with open(self.default_config_file, 'w') as configfile:
                    self.config.write(configfile)
            config = self.default_config_file
        self.config.read(config)

    def get(self, key: str, default: Any = None) -> Any | None:
        """
        Return a configuration parameter associated with the key.
        :param key:
        :return:
        """
        if key in self.config[self.section]:
            return self.config[self.section][key]

        return default

    def set(self, key: str, value: Any) -> None:
        """
        Set the configuration value for the specified key to the given value.
        :param key:
        :param value:
        :return:
        """
        self.config[self.section][key] = str(value)

    def create_default_config(self):
        """
        Sets default config values.
        :return:
        """
        self.config[self.section] = {
            'HeartbeatInterval': '30',
        }
