from typing import Protocol, Any


class ClientConfig(Protocol):
    """
    Defines the protocol for supplying and retrieving Client configuration values.
    """
    def get(self, key: str, default: Any = None) -> Any | None:
        """
        Return a configuration parameter associated with the key.
        :param key:
        :return:
        """

    def set(self, key: str, value: Any) -> None:
        """
        Set the configuration value for the specified key to the given value.
        :param key:
        :param value:
        :return:
        """


class Client:
    """
    The base client class holding all logic and functionality for an individual client.
    """
    def __init__(self, config: ClientConfig) -> None:
        self.config = config
